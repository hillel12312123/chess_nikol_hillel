#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));
	Board board = Board();
	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	

	strcpy_s(msgToGraphics, "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr0"); 

	p.sendMessageToGraphics(msgToGraphics);  
	
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		std::string answer = board.sendMove(msgFromGraphics);
		strcpy_s(msgToGraphics, answer.c_str()); 

		 
		


	
		p.sendMessageToGraphics(msgToGraphics);   

		
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
	board.~Board();
}