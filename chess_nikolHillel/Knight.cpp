#include "Knight.h"

/*
constructor of Knight.
input: name, type and position of Piece.
output: none.
*/
Knight::Knight(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation) {}

Knight::~Knight()//dtor
{

}


/*
this function returns if the Knight can reach given position
input: position to check.
output: true or false
*/

int Knight::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[SIZE_BOARD][SIZE_BOARD])
{
	int xMove = abs(xDestPosition - this->xLocation);
	int yMove = abs(yDestPosition - this->yLocation);

	if ((DOUBLE_PARSE == xMove && PARSE==yMove) || (PARSE==xMove  && DOUBLE_PARSE == yMove))
	{
		return LEGAL_MOVE; // valid move
	}
	return ILLEGAL_MOVE; // invalid move
}

