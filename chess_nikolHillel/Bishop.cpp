#include "Bishop.h"
//bishop ctor 
Bishop::Bishop(const char name, const int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
}
Bishop::~Bishop()//bishop dtor
{
}


/*
this function returns if the rook can reach given position
input: position to check.
output: true or false
*/
int Bishop::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[SIZE_BOARD][SIZE_BOARD])
{
	int indexY = 0,indexX=0,incOrdecY=0,i=0,incOrdecX=0;
	//check if the target block is an diagonal line if it is the player will get an error
	if (std::abs(this->yLocation-yDestPosition)!= std::abs(this->xLocation- xDestPosition))
	{
		return ILLEGAL_MOVE;
	}
	incOrdecX= xDestPosition < this->xLocation? LESS_ONE_LOCTION : MORE_ONE_LOCTION;
	incOrdecY = yDestPosition < this->yLocation ? LESS_ONE_LOCTION : MORE_ONE_LOCTION;
	//update the position for next check
	indexY = this->yLocation +incOrdecY;
	indexX = this->xLocation + incOrdecX;
	// a loop that check if their is a piece in the path of the target
	for (i = 0; i < std::abs(this->xLocation - xDestPosition)+LESS_ONE_LOCTION; i++)
	{
		if (nullptr!=board[indexY][indexX])//if their is a piece in the way it will send an error
		{
			return ILLEGAL_MOVE;
		}
		indexY += incOrdecY;
		indexX += incOrdecX;
	}
	return LEGAL_MOVE;//if none of the ifs are in use it will return 0 which mean legal move
}
