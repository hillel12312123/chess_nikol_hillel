#pragma once
#include "Piece.h"
class Pawn :public Piece
{
public:
	Pawn(char name, int type, int xLocation, int yLocation);
	~Pawn();
	//check if the the pawn can reach the choosen block with his way of moving
	int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD]) override;
	//get from the function if this is the first move for pawn
	bool getFirstMove();
	void setFirstMove(bool FirstPawnMove);
private:
	bool FirstMove;
};

