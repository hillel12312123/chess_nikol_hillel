#include "Queen.h"

/*
constructor of Queen.
input: name, type and position of Piece.
output: none.
*/
Queen::Queen(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
    // The constructor simply calls the constructor of the Piece class
}
Queen::~Queen()// queen dtor
{
}

// This function checks if the queen can move to a given position
int Queen::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD])
{
    // Create a bishop and a rook with the same properties as the queen
    Bishop bishop(this->name, this->type, this->xLocation, this->yLocation);
    Rook rook(this->name, this->type, this->xLocation, this->yLocation);

    // The queen can move like a bishop or a rook
    // If the move is valid for the bishop or the rook, it is valid for the queen
    if (LEGAL_MOVE ==bishop.isLegitMove(xDestPosition, yDestPosition, board) ||LEGAL_MOVE== rook.isLegitMove(xDestPosition, yDestPosition, board))
    {
        return LEGAL_MOVE; // Return 0 to indicate a valid move
    }
    return ILLEGAL_MOVE; // If the move is not valid for either the bishop or the rook, it is not valid for the queen
}


