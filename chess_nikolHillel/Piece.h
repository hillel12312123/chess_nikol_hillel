#pragma once
#include <string>
#define SIZE_BOARD 8
#define BLACK 1
#define WHITE 0
#define X_PLACE 1
#define PLACE 2
#define Y_PLACE 0
#define LEGAL_MOVE 0
#define ILLEGAL_INDEX 5
#define ILLEGAL_MOVE 6
#define MIN 0
#define LESS_ONE_LOCTION -1
#define MORE_ONE_LOCTION 1
#define PARSE 1
#define DOUBLE_PARSE 2
/////////////////////////////////////////////////////////////////////
///    the abstratic code for all the functions for the pieces    ///
/////////////////////////////////////////////////////////////////////

using std::string;
using std::to_string;

class Piece
{
public:
	//c'tor
	Piece(char name, int type, int xLocation, int yLocation);
	 virtual ~Piece();
	//setters and getters
	void setName(const char  name);			//set the name for the piece 
	void setType(const int type);			//set the type for the piece
	int get_xLocation();					//get from the board the location of x of the piece
	int get_yLocation();					// get the y location of piece
	void set_yLocation(int yLocation);		//set the y location for the piece
	void set_xLocation(int xLocation);		//swt the x location of the piece

	char  getName() const;	//set the name by P p Q q B b...
	int getType() const;
	//other methods
	virtual int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD])=0;		//abstratic function for all the pieces to check if the move is legit for the specific piece
	bool operator==(const Piece& other) const;

protected:
	char name;  
	int type;
	int xLocation;
	int yLocation;
};
