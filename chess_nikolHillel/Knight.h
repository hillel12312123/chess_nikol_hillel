#pragma once
#include "Piece.h"

class Knight: public Piece
{
public:
	//methods
	Knight(char name, int type, int xLocation, int yLocation);
	~Knight();
	//check if the the knight can reach the choosen block with his way of moving
	int isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD])override;
};