#include "King.h"
//ctor
King::King(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{

}
King::~King()//dtor
{
}

// This function checks if the king can move to a given position
int King::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD])
{
    // The king can only move one square in any direction
    // If the absolute difference in the x or y coordinates is not 1, the move is not valid
    if (!(PARSE == std::abs(xDestPosition - this->xLocation) || PARSE == std::abs(yDestPosition - this->yLocation)))
    {
        return ILLEGAL_MOVE; // Return 6 to indicate an invalid move
    }
    if (std::abs(this->yLocation - yDestPosition) == std::abs(this->xLocation - xDestPosition) || (this->xLocation == xDestPosition || this->yLocation == yDestPosition))
    {
        return LEGAL_MOVE; // If the move is valid, return 0
    }
    return ILLEGAL_MOVE;
}



