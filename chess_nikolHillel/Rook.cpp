#include "Rook.h"

/*
constructor of Rook.
input: name, type and position of Piece.
output: none.
*/
Rook::Rook(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
}
Rook::~Rook() // rook dtor
{
}


/*
this function returns if the rook can reach given position
input: position to check.
output: true or false
*/
// This function checks if the rook can move to a given position
int Rook::isLegitMove(int xDestPosition, int yDestPosition,Piece* board[SIZE_BOARD][SIZE_BOARD])
{
	int index = 0,add =0;
	// The rook can only move in a straight line horizontally or vertically
	// If the rook is not moving in a straight line, return 6
	if (this->xLocation != xDestPosition && this->yLocation != yDestPosition)
	{
		return ILLEGAL_MOVE;
	}
	// If the rook is moving horizontally
	if (this->xLocation != xDestPosition)
	{
		// Determine the direction of movement
		index = xDestPosition > this->xLocation ? this->xLocation + MORE_ONE_LOCTION : this->xLocation+LESS_ONE_LOCTION;
		add = xDestPosition > this->xLocation ? MORE_ONE_LOCTION: LESS_ONE_LOCTION;
		// Check all the squares on the way to the destination
		for (int j = 0; j < std::abs(xDestPosition - this->xLocation)+LESS_ONE_LOCTION; j++)
		{
			// If there is a piece on the square, return 6
			if (nullptr!=board[yLocation][index])
			{
				return ILLEGAL_MOVE;
			}
			// Update the index for the next check
			index += add;
		}
	}
	else // If the rook is moving vertically
	{
		// Determine the direction of movement
		index = yDestPosition > this->yLocation ? this->yLocation + MORE_ONE_LOCTION : this->yLocation + LESS_ONE_LOCTION;
		add = yDestPosition > this->yLocation ? MORE_ONE_LOCTION : LESS_ONE_LOCTION;
		// Check all the squares on the way to the destination
		for (int j = 0; j < std::abs(yDestPosition - this->yLocation)+LESS_ONE_LOCTION; j ++)
		{
			// If there is a piece on the square, return 6
			if (board[index][xLocation] != nullptr) 
			{
				return ILLEGAL_MOVE;
			}
			// Update the index for the next check
			index += add;
		}
	}
	return LEGAL_MOVE; // If the move is valid, return 0
}
