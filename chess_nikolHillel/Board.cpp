#include "Board.h"
#include <iostream>
Board::Board()//ctor
{
    const std::string BOARD_STR = "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr";//string for the build of the board
    int i = 0;
    for (i=0;i< BOARD_STR.size();i++)
    {
        switch (BOARD_STR[i])//build the pieces of the board
        {
        case 'R':
        {

            board[i % SIZE_BOARD][i /SIZE_BOARD] = new Rook('R', WHITE, i/SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'r':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Rook('r', BLACK, i /SIZE_BOARD , i %SIZE_BOARD);
            break;
        }
        case 'k':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new King('k', BLACK, i /SIZE_BOARD, i %SIZE_BOARD);
            break;
        }
        case 'K':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new King('K', WHITE, i /SIZE_BOARD, i %SIZE_BOARD);
            break;
        }
        case 'p':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Pawn('p', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'P':
        {
            board[i %SIZE_BOARD][i / SIZE_BOARD] = new Pawn('P', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'b':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Bishop('b', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'B':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Bishop('B', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'n':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Knight('n', BLACK, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'N':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Knight('N', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'q':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Queen('q', BLACK, i /SIZE_BOARD, i % SIZE_BOARD);
            break;
        }
        case 'Q':
        {
            board[i % SIZE_BOARD][i / SIZE_BOARD] = new Queen('Q', WHITE, i / SIZE_BOARD, i % SIZE_BOARD);
            break;
        }

        default:
            board[i % SIZE_BOARD][i / SIZE_BOARD] = nullptr;
            break;
        }
    }
    this->isWhiteTurn = true;
    
}
void Board::decrypted(std::string strLocation, int* location)//dectpted the mseege from the graphic to the board
{
    const char GET_THE_LOCTION_LETTER = 'a', GET_LOCTION_NUM = '8';
    location[Y_PLACE] = strLocation[Y_PLACE] - GET_THE_LOCTION_LETTER;
    location[X_PLACE] = std::abs(strLocation[X_PLACE] - GET_LOCTION_NUM);
}

std::string Board::sendMove(std::string message)//move the piece in the board
 {
        int source[PLACE] = { 0 };
        std::string location = "";
        Piece* piece = nullptr;

        location += message[0];
        location += message[1];
        decrypted(location, source);

        // Check if indices are within the valid range
        if (MIN > source[Y_PLACE] && SIZE_BOARD<source[Y_PLACE] && MIN> source[X_PLACE] && SIZE_BOARD < source[X_PLACE])
        {
            return to_string(ILLEGAL_INDEX);
        }
        else
        {
            piece = this->board[source[Y_PLACE]][source[X_PLACE]];
            location.clear();
            location += message[2];
            location += message[3];
            decrypted(location, source);
            return move(piece, source[X_PLACE], source[Y_PLACE]);
        }

}



std::string Board::move(Piece* piece,int xLoction,int yLoction)//check with the choosen piece if the move is valid to him
{
    int res = 0,opType=0,xSource=0, ySource = 0;
    if (nullptr == piece)
    {
        return to_string(SOURCE_NOT_REACABLE);
    }
    {
        if (WHITE == piece->getType())//check for what team the piece belong to
        {
            opType = BLACK;
        }
        else
        {
            opType = WHITE;
        }
        // Check if it's the white's turn
        if (this->isWhiteTurn)
        {
            // If the piece is not white, return 2
            if (BLACK!=opType)
            {
                return to_string(SOURCE_NOT_REACABLE);
            }
        }
        else
        {
            // If the piece is white, return 2
            if (WHITE != opType)
            {
                return to_string(SOURCE_NOT_REACABLE);
            }
        }
        res = canMove(xLoction, yLoction, piece);
        if (LEGAL_MOVE == res)
        {
            if (nullptr != this->board[yLoction][xLoction])
            {
                if (KING == (char)tolower(this->board[yLoction][xLoction]->getName()))
                {
                    return to_string(ILLEGAL_MOVE);
                }
                eatPiece(xLoction, yLoction);
            }

            xSource = piece->get_xLocation();
            ySource = piece->get_yLocation();
            doMove(ySource, xSource, xLoction, yLoction, piece);
            if (isKingChecked(piece->getType(), opType))//selfCheck
            {
                doMove(yLoction, xLoction ,xSource, ySource, piece);
                return to_string(SELF_CHECK);
            }
            if (isKingChecked(opType, piece->getType()))//check if from the given move the other player king got checked
            {
                res = CHECK;
            }
            this->isWhiteTurn = !this->isWhiteTurn;
        }
        return to_string(res);
    }
    
}

void Board::doMove(int ySource, int xSource, int xDest, int yDest,Piece* piece)
{
    this->board[ySource][xSource] = nullptr;
    this->board[yDest][xDest] = piece;
    piece->set_xLocation(xDest);
    piece->set_yLocation(yDest);

}
int Board::eatPiece(int xPosition, int yPosition) // This function removes a piece from the board
{
    if (MIN > xPosition && SIZE_BOARD < xPosition &&MIN> yPosition&& SIZE_BOARD < yPosition)// Check if the position is within the board
    {
        return ILLEGAL_INDEX;
    }
    else
    {
        Piece* pieceToBeEaten = this->board[yPosition][xPosition];        // Get the piece at the given position
        if (nullptr == pieceToBeEaten)
        {
            return ILLEGAL_MOVE;
        }
        else
        {
            delete pieceToBeEaten;            // Delete the piece

            this->board[yPosition][xPosition] = nullptr;            // Set the position on the board to null

            return LEGAL_MOVE;//send that the move is valid
        }
    }
}
// This function checks if a piece can move to a given position on the board
int Board::canMove(int xPosition, int yPosition, Piece* piece)
{
    // Check if the piece is not null
    if (nullptr == piece)
    {
        return SOURCE_NOT_REACABLE;
    }
    else
    {
        // If there is a piece at the destination
        if (this->board[yPosition][xPosition] != nullptr)
        {
            // If the piece at the destination is of the same type, return 3
            if (this->board[yPosition][xPosition]->getType() == piece->getType())
            {
                return CANNOT_REACH_DESTINTION;
            }
        }
        // If the piece is already at the destination, return 7
        if (this->board[yPosition][xPosition] == piece)
        {
            return SAME_PLACE;
        }
        // Check if the move is legitimate for the piece
        return piece->isLegitMove(xPosition, yPosition, this->board);
        
    }
}

// This function checks if the king of a given type is checked by any of the opponent's pieces
bool Board::isKingChecked(int typeKing,int typeSoldiers)
{
    int i = 0, j = 0,xKingLoction=0, yKingLoction=0;
    bool found = false,firstMove =false; // Initialize found to false
    int* kingLocation = nullptr; // Initialize the king's location to null
    kingLocation = getKingLocation(typeKing); // Get the king's location
    xKingLoction = kingLocation[X_PLACE];
    yKingLoction = kingLocation[Y_PLACE];
    // Iterate over the board
    for (i = 0; SIZE_BOARD> i && !found; i++)
    {
        for (j = 0;SIZE_BOARD> j && !found; j++)
        {
            // If there is a piece at the current postion
            if (nullptr!=this->board[j][i])
            {
                // If the piece is of the type of the soldiers
                if (this->board[j][i]->getType() == typeSoldiers)
                {
                    // If the piece can move to the king's location, set found to true
                    if (LEGAL_MOVE== canMove(xKingLoction, yKingLoction, this->board[j][i]))
                    {
                        found = true;
                    }
                }
            }
        }
    }
    // Return whether the king is checked
    return found;
}


// This function gets the location of the king of a given type
int* Board::getKingLocation(int type)
{
    int i = 0, j = 0;
    int location[PLACE] = { 0 }; // Initialize the location to 0

    // Iterate over the board
    for (i = 0; SIZE_BOARD>i; i++)
    {
        for (j = 0;SIZE_BOARD> j; j++)
        {
            // If there is a piece at the current position
            if (this->board[j][i] != nullptr)
            {
                // If the piece is a king of the given type
                if (this->board[j][i]->getType() == type && KING==(char)tolower(this->board[j][i]->getName()))
                {
                    // Get the location of the king
                    location[Y_PLACE] = this->board[j][i]->get_yLocation();
                    location[X_PLACE] = this->board[j][i]->get_xLocation();
                }
            }
        }
    }

    // Return the location of the king
    return location;
}
Board::~Board()//dtor  
{
    int i = 0,j=0;
    Piece* piece = nullptr;
    for (i = 0; SIZE_BOARD > i; i++)
    {
        for (j = 0; SIZE_BOARD > j; j++)
        {
                piece = this->board[j][i];
                delete piece;
                this->board[j][i] = nullptr;
            
        }
    }
}

