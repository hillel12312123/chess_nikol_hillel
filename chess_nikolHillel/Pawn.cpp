#include "Pawn.h"

// Constructor for the Pawn class
Pawn::Pawn(char name, int type, int xLocation, int yLocation) : Piece(name, type, xLocation, yLocation)
{
	this->FirstMove = true; // Initialize FirstMove to true
}
Pawn::~Pawn()//dtor
{
}

// This function checks if the pawn can move to a given position
int Pawn::isLegitMove(int xDestPosition, int yDestPosition, Piece* board[SIZE_BOARD][SIZE_BOARD])
{
	int parse = 0, incOrDec = 0;
	// If the destination square is empty
	if (nullptr == board[yDestPosition][xDestPosition])
	{
		// If the pawn is not moving vertically, return 6
		if (yDestPosition != this->yLocation)
		{
			return ILLEGAL_MOVE;
		}
	}

	else // If the destination square is not empty
	{
		
		// If the pawn is not moving diagonally, return 6
		if (PARSE != std::abs(xDestPosition - this->xLocation) && PARSE != std::abs(yDestPosition - this->yLocation) || std::abs(yDestPosition - this->yLocation) != std::abs(xDestPosition - this->xLocation))
		{
			return ILLEGAL_MOVE;
		}
	}
	// Calculate the number of squares the pawn is moving forward
	parse = WHITE == this->type ? xDestPosition - this->xLocation : this->xLocation - xDestPosition;
	// Determine the direction of movement
	incOrDec = WHITE == this->type ? MORE_ONE_LOCTION : LESS_ONE_LOCTION;
	// If the pawn is not moving forward, return 6//
	if (MIN >= parse)
	{
		return ILLEGAL_MOVE;
	}
	if (this->get_yLocation() != yDestPosition)
	{
		if (std::abs(xDestPosition - this->xLocation) != std::abs(yDestPosition - this->yLocation))
		{
			return ILLEGAL_MOVE;
		}
	}
	else if (nullptr != board[this->yLocation][this->xLocation + incOrDec])
	{
		return ILLEGAL_MOVE;
	}
	// If it's the pawn's first move      
	if (this->FirstMove)
	{
		// If the pawn is moving more than two squares forward, return 6
		if (DOUBLE_PARSE <parse ||(nullptr != board[this->yLocation][this->xLocation + (incOrDec * DOUBLE_PARSE)]  && DOUBLE_PARSE == parse ))
		{
			return ILLEGAL_MOVE;
		}
	}
	else // If it's not the pawn's first move
	{
		// If the pawn is moving more than one square forward, return 6
		if (PARSE!=parse)
		{
			return ILLEGAL_MOVE;
		}
	}
	// If it's the pawn's first move, set FirstMove to false
	if (this->FirstMove)
	{
		this->FirstMove = !this->FirstMove;
	}
	return LEGAL_MOVE; // If the move is valid, return 0
}

// This function returns the value of FirstMove
bool Pawn::getFirstMove()
{
	return this->FirstMove;
}

// This function sets the value of FirstMove
void Pawn::setFirstMove(bool FirstPawnMove)
{
	this->FirstMove = FirstPawnMove;
}
