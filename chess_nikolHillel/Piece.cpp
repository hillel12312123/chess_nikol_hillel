#include "Piece.h"
#include <stdio.h>

/*
c'tor of Piece
input: name of piece, piece's type and piece's current position on the board.
output: none.
*/
Piece::Piece(char name, int type, int xLocation, int yLocation)
{
	this->name = name;
	this->type = type;
	this->xLocation = xLocation;
	this->yLocation = yLocation;

}


void Piece::set_xLocation(int xLocation) // set the x location
{
	this->xLocation=xLocation;
}
void Piece::set_yLocation(int yLocation ) //set the y location
{
	this->yLocation= yLocation;
}
int Piece::get_xLocation() // get the x location
{
	return this->xLocation;
}
int Piece::get_yLocation()// get the y location
{
	return this->yLocation;
}
/*
this function changes the name of the piece.
input: new name of the piece.
output: none.
*/
void Piece::setName(const char name)
{
	this->name = name;
}

/*
this function changes the type of the piece.
input: new type of the piece.
output: none.
*/
void Piece::setType(const int type)
{
	this->type = type;
}

/*
this function returns the name of the piece.
input: none.
output: name of the piece.
*/
char  Piece::getName() const
{
	return this->name;
}


/*
this function returns the type of the piece.
input: none.
output: type of the piece.
*/
Piece::~Piece()
{
	name = ' ';
	type = 0;
	xLocation = 0;
	yLocation = 0;
}
int Piece::getType() const
{
	return this->type;
}
// overload the operator == to comper between to pieces 
bool Piece::operator==(const Piece& other) const
{
	return this->name == other.name && this->type == other.type&& other.xLocation==this->xLocation&& other.yLocation == this->yLocation;
}

