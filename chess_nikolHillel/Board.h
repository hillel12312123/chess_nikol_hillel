#pragma once
#include <map>
#include<iostream>
#include "King.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Piece.h"
#define SIZE_BOARD 8
#define BLACK 1
#define WHITE 0
#define X_PLACE 1
#define PLACE 2
#define Y_PLACE 0
#define LEGAL_MOVE 0
#define CHECK 1
#define SOURCE_NOT_REACABLE 2
#define CANNOT_REACH_DESTINTION 3
#define SELF_CHECK 4
#define ILLEGAL_INDEX 5
#define ILLEGAL_MOVE 6
#define SAME_PLACE 7
#define MATE 8
#define KING 'k'
#define MIN 0
class Board
{
private:
     Piece* board[SIZE_BOARD][SIZE_BOARD];//linking between the piece code and the board
    bool isWhiteTurn;
public:
    ~Board();
    Board();
    void decrypted(std::string strLocation, int* location);
    std::string sendMove(std::string messege);
    int eatPiece(int xPosition, int yPosition);
    int canMove(int xPosition, int yPosition, Piece* piece);//check uf the nove is legit by using the is legit move in each piece


    std::string move(Piece* piece, int xLoction, int yLoction);
    
    void doMove(int ySource, int xSource, int xDest, int yDest, Piece* piece);

    bool isKingChecked(int typeKing, int typeSoliders);//check if the king got checked
    

    int* getKingLocation(int type);// get the location for the king(work with white and black) used for checking check 
};
