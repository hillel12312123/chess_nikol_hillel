#pragma once
#include "Piece.h"
#include <cmath>
class Bishop: public Piece
{
public:
    Bishop(char name, int type, int xLocation, int yLocation);//ctor for the piece bishop
    ~Bishop();
    // Override the isLegitMove function from the base class
    //check if the bishop can reach the target block
    int isLegitMove(int xDestPosition, int yDestPosition,Piece* board[SIZE_BOARD][SIZE_BOARD]) override;
};

